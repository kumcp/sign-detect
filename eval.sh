   
. ~/project/web/python-django/train-project/testenv/bin/activate

cd /home/kum/project/ML/models/research/

python object_detection/eval.py \
    --logtostderr \
    --pipeline_config_path=/home/kum/project/ML/sign/running.config \
    --checkpoint_dir=/home/kum/project/ML/sign/traindir \
    --eval_dir=/home/kum/project/ML/sign/evaldir/
