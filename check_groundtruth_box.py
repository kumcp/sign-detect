import cv2
import csv


def draw_box_infile(image_path, output, groundtruth_file):
    """Draw box from groundtruth file with format:
    filename.ext;x1;y1;x2;y2;class_id
    Args:
        image_path: original image file
        output: output file (with box)
        groundtruth_file: directory to the groundtruth file
    """
    # read image
    image = cv2.imread(image_path)
    image_name = image_path.split("/")[-1].split(".")[0]
    print("Height: " + str(len(image)))
    print("Width: " + str(len(image[0])))
    # read bb
    with open() as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            if image_name == row[0].split('.')[0]:
                print(row)
                cv2.rectangle(image, (int(row[1]), int(row[2])), (int(
                    row[3]), int(row[4])), (0, 255, 0), 2)

    cv2.imwrite(output, image)
    cv2.imshow(output)


def draw_box(image_path, output, box, type=1, color=(0, 255, 0), stroke_width=2):
    """Draw box to image
    Args:
        image_path:
        output:
        box: 4item-array contain [x1,y1,w,h] by default. All value must by int (pixel) or float (pixel/width-height)
        type: 
            1: when box is [x1,y1,w,h] (common)
            2: when box is [x1,y1,x2,y2] (drawing)
            3: when box is [y1,x1,y2,x2] (tensorflow)
        color:
        stroke_width:
    """
    print(image_path)
    image = cv2.imread(image_path)
    image_name = image_path.split("/")[-1].split(".")[0]

    im_w = len(image[0])
    im_h = len(image)

    float_type = False
    if not all(isinstance(item, int) for item in box):
        float_type = True
        if not all(isinstance(item, float) for item in box):
            raise TypeError("box must be all int or float")

    if type == 2:
        x1, y1, x2, y2 = box
    elif type == 3:
        y1, x1, y2, x2 = box
    else:
        x1, y1, w, h = box
        x2 = x1 + w
        y2 = y1 + h

    if float_type:
        x1 = int(x1*im_w)
        y1 = int(y1*im_h)
        x2 = int(x2*im_w)
        y2 = int(y2*im_h)

    cv2.rectangle(image, (x1, y1), (x2, y2), color, stroke_width)

    cv2.imwrite(output, image)
    # cv2.imshow("a",image)
    # cv2.waitKey(0)
