from check_groundtruth_box import draw_box

import csv

def compute_iou_boxes(box1, box2):
    """Compute Intersect over Union (IoU) between 2 boxes.

    Args:
        box1, box2: tupple/list/array contain x, y, width, height of a rectangle box.

    Return -> (float) IOU value
    """

    x1_inter = max(box1[0], box2[0])
    y1_inter = max(box1[1], box2[1])

    x2_inter = min(box1[0] + box1[2], box2[0] + box2[2])
    y2_inter = min(box1[1] + box1[3], box2[1] + box2[3])

    w_inter = x2_inter - x1_inter
    h_inter = y2_inter - y1_inter

    if w_inter <0 or h_inter < 0:
        num = -1.0
    else:
        num = 1.0

    S_inter = w_inter * h_inter
        
    S_box1 = box1[2] * box1[3]
    S_box2 = box2[2] * box2[3]
    

    iou = float(S_inter) / float(S_box1 + S_box2 - S_inter)

    return num * abs(iou)

def __default_load_csv_logic(row):
    return row[0].split(".")[0], row

def load_groundtruth_file(gt_file, modify_logic=__default_load_csv_logic):
    rowlist = {}
    with open(gt_file) as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            id_row, new_row = modify_logic(row)
            if id_row in rowlist:
                rowlist[id_row].append(new_row)
            else:
                rowlist[id_row] = [new_row]

    return rowlist


def get_detect_box(boxes, width, height):
    detect_box = []
    detect_box.append(int(boxes[1] * width))
    detect_box.append(int(boxes[0] * height))
    detect_box.append(int((boxes[3] - boxes[1]) * width))
    detect_box.append(int((boxes[2] - boxes[0]) * height))
    return detect_box

def filter_detect_score(image_path, detect_function, threshold=0.5):
    boxes, scores, classes, num, width, height = detect_function(image_path)
   
    detected = []

    for index in range(len(scores[0])):
        if scores[0][index] > threshold:
            detected.append({
                "box": get_detect_box(boxes[0][index], width, height),
                "score": scores[0][index],
                "class": int(classes[0][index])
            })
        else:
            break
    return detected

def check_detect_image(image_path, groundtruth, detect_function, threshold=0.5):
    """Show fail detect image
    Args:
        image_path:
        grounttruth: groundtruth_dictionary as flow x,y,w,h,class
        threshold: only check result has score above threshold
        detect_function: detect function get input image and return 
            boxes, scores, classes, num, width, height
    Return:
        Array contain wrong_detect image and type of wrong detect
    """
    wrong_detect = []

    detected = filter_detect_score(image_path, detect_function, threshold)

    for gt_i in range(len(groundtruth)):
        gt_x, gt_y, gt_w, gt_h = groundtruth[gt_i][0:4]
        gt_box = [int(gt_x), int(gt_y), int(gt_w), int(gt_h)]
        gt_class = groundtruth[gt_i][4]
        dt_index = 0
        max_iou = 0
        
        
        for dt_i in range(0,len(detected)):
            iou = compute_iou_boxes(detected[dt_i]["box"], gt_box)
            
            if max_iou < iou:
                max_iou = iou
                dt_index = dt_i
        
        if len(detected) == 0 or max_iou <=0.1:
            print("Missing detect class {}: ".format(gt_class))
            print(gt_box)
            wrong_detect.append({
                "path": image_path,
                "mes": "Missing detect class {} - {}".format(gt_class, gt_box)
            })

        
        if max_iou <= 0.5 and max_iou>0.1:
            print("IOU is too low: {}".format(max_iou))
            print(gt_box)
            print(detected[dt_index])
            wrong_detect.append({
                "path": image_path,
                "mes": "IOU is too low {} - {} - {}".format(max_iou ,detected, gt_box)
            })
            detected.remove(detected[dt_index])
            
        if max_iou >0.5:        
            
            if int(gt_class) == detected[dt_index]["class"]:
                
                print("Right class")
            else:
                wrong_detect.append({
                    "path": image_path,
                    "mes": "Wrong class classify: {} but is {}".format(detected[dt_index]["class"], gt_class)
                })
                print("Wrong class classify: {}".format(detected[dt_index]["class"]))
            detected.remove(detected[dt_index])

    if len(detected)>0:
        for detect in detected:
            print("Wrong detect: ")
            print(detect)
            wrong_detect.append({
                "path": image_path,
                "mes": "Wrong detect: {}".format(detect['class'])
            })
    return wrong_detect

def save_wrong_detect(wrong_detect, output_file):
    with open(output_file, 'wb') as csvfile:
        writer = csv.writer(csvfile)
        for detect in wrong_detect:
            writer.writerow([detect["path"], detect["mes"]])
    