
from check_groundtruth_box import draw_box
from check_detect import check_detect_image, load_groundtruth_file, filter_detect_score, save_wrong_detect
import cv2
import os
from sign_detection import detect


def show_image_box(box, image_path):
    draw_box(image_path, "/home/kum/a.jpg", box, type=1)


def show_image_detect(image_name):
    image_path = "/media/kum/DATA/export_sign/raw/trans/" + image_name + ".jpg"
    detected = filter_detect_score(image_path, detect)
    a = 0
    for dt in detected:
        print(dt)
        a += 1
        image_path = "/media/kum/DATA/export_sign/raw/trans/" + image_name + ".jpg"
        draw_box(image_path, "/home/kum/dt-" + str(a) + ".jpg", dt["box"], type=1)


def show_image_gt(image_name):
    gt_boxes = load_groundtruth_file(
        "/media/kum/DATA/export_sign/raw/data/gt_or.txt", load_box_logic)
    a = 0
    for box in gt_boxes[image_name]:
        a += 1
        print(box)
        image_path = "/media/kum/DATA/export_sign/raw/trans/" + image_name + ".jpg"
        draw_box(image_path, "/home/kum/gt-" + str(a) + ".jpg", box[0:4], type=1)

# show_image_groundtruth()

def load_box_logic(row):
    x1, y1, x2, y2 = [int(i) for i in row[1:5]] 
    box = [x1, y1, x2 - x1, y2 - y1, row[5]]
    return row[0].split(".")[0], box

def check_detect(filename):
    wrong_detect = []
    # for i in [filename+".jpg"]:
    for i in os.listdir("/media/kum/DATA/export_sign/raw/trans/"):
        image_path = "/media/kum/DATA/export_sign/raw/trans/{}".format(i)
        print("Checking: {}".format(image_path))
        gt_boxes = load_groundtruth_file(
            "/media/kum/DATA/export_sign/raw/data/gt_or.txt", load_box_logic)
        if i.rsplit(".", 1)[0] in gt_boxes:
            fail = check_detect_image(image_path, gt_boxes[i.rsplit(".", 1)[0]], detect, 0.5)
            if len(fail) > 0:
                wrong_detect += fail

    save_wrong_detect(wrong_detect, "error_detect_ssd_27122017_0.5.csv")
    return wrong_detect


# wrong_detect = check_detect("00822")
# print(wrong_detect)
show_image_gt("00723")
show_image_detect("00723")
