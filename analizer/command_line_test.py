from analizer import load_groundtruth_file, class_image_summary
import functools

def load_class_only_logic(row, position):
    
    return row[0].split(".")[0], row[position]

def analyze():
    position = 5
    gt_boxes = load_groundtruth_file(
            "/media/kum/DATA/export_sign/raw/data/gt_or.txt", functools.partial(load_class_only_logic, position=position))
    
    result = class_image_summary(gt_boxes, 1)
    


analyze()