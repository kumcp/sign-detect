import csv

def class_image_summary(groundtruth, class_position):
    """Get summary and distribution from groundtruth csv file.
    Args:
        groundtruth_file: file directory
        class_position: column position in csv file.
    Return:
        summary: dictionary contain image number of class, percent, ...
    """
    summary = {}
    for file_name, class_list in groundtruth.items():
        for class_label in class_list:
            if class_label in summary:
                summary[class_label] += 1
            else:
                summary[class_label] = 1
    
    sorted_class = sorted(summary, key=lambda class_name: summary[class_name], reverse=True)
    for item_sorted in sorted_class:
        print("{} - {}".format(item_sorted, summary[item_sorted]))
    return sorted_class


def __default_load_csv_logic(row):
    return row[0].split(".")[0], row

def load_groundtruth_file(gt_file, modify_logic=__default_load_csv_logic):
    rowlist = {}
    with open(gt_file) as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            id_row, new_row = modify_logic(row)
            if id_row in rowlist:
                rowlist[id_row].append(new_row)
            else:
                rowlist[id_row] = [new_row]

    return rowlist